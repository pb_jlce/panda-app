## About this demo app

Check the file "INSTRUCTIONS.pdf" in "extras" directory into the project root.

- The app was developed with PHP 8.2.4 as the base backend language, under the Laravel 10.14.1 framework.
- Logically as Laravel project is already working with composer and a PSR-4 structure throughout the project.
- A unit testing has been included through the PHPUnit package that Laravel already includes.
- The API returns data in JSON format.
- The API it has been documented with a package for Swagger/OpenAPI.

The API documentation can be consulted either in the frontend of the application ("/api/documentation") or through the API itself in the endpoint "/docs/api-docs.json".

A POSTMAN collection is supplied (in the "extras" directory of the project root). An application database diagram is also attached to this directory.

## Version control

I've used git as VCS. And I used GitLab as a remote repository.

## Instructions

In order to download the project and run it correctly, the operating system from which you proceed must have PHP 8.2, Composer and Git CVS installed and correctly configured. I developed, deployed and tested the application under a MS Windows 10 Pro x64 operating system.

Steps to mount the application locally and correctly:

Clone the project from the URL:  https://gitlab.com/pb_jlce/panda-app.git
Having git installed, if you are on Windows, it is highly recommended to start git BASH and run the next commands already from this console.

Access the directory of the cloned project. For example using the command "cd example-path/panda-app".

Execute the command "composer install" in the root of the cloned project. This will install all Composer dependencies that are specified in the "composer.json" file. These dependencies will be stored in the vendor/ directory.

Duplicate the ".env.example" file and rename it to ".env".
Generate a new application key by running the command "php artisan key:generate". This will generate a new encryption key for the application and automatically add it to the ".env" file.

Now generate a local sqlite database file. Initially create a blank "database.sqlite" file in the project's "database" directory. With the git console (git BASH) you can run the command "touch database/database.sqlite" to perform this action properly.

Execute the command "php artisan migrate". To generate the local database structure.
Now run the command "php artisan db:seed --class=DatabaseSeeder" to populate with records the already created tables.

To make sure that everything is set up as it should be, it is possible to launch the unit tests programmed in the application: this is done by executing the command "php artisan test".

Now all that remains is to run, so to speak, the application: run the "php artisan serve" command and run, if desired, functional tests with POSTMAN (example collection supplied), to test the various API endpoints of the application.

The API requires authentication, so the first thing to do is to launch the POST login, which is in the "Login" directory of the POSTMAN collection, to generate a valid token for the user that is already being proposed in this request.
From this point on, it is possible to test all the other endpoints of the application, remembering to indicate the authentication Bearer Token generated.

Another option for testing the application is from the web browser, by accessing the local URL "http://localhost:8000/api/documentation": from there it is possible to test the API endpoints, after authentication (on the web page that opens, there is a button in the top centre right-hand corner that opens a pop-up window in which to indicate the authenticated Bearer token).



