<?php

namespace App\Http\Controllers;

use App\Models\Rating;
use App\Models\Vehicle;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

/**
 * Class VehicleController
 * @package App\Http\Controllers
 *
 * @OA\Tag(
 *     name="Vehicles",
 *     description="Endpoints for managing vehicles"
 * )
 */
class VehicleController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/vehicles",
     *     tags={"Vehicles"},
     *     summary="Get list of vehicles",
     *     description="Get a list of all vehicles with optional filtering, sorting and pagination",
     *     security={{"sanctum": {}}},
     *     @OA\Parameter(
     *         name="with_relations",
     *         in="query",
     *         description="Include related entities in response",
     *         required=false,
     *         @OA\Schema(type="boolean")
     *     ),
     *     @OA\Parameter(
     *         name="paginate",
     *         in="query",
     *         description="Paginate results",
     *         required=false,
     *         @OA\Schema(type="boolean")
     *     ),
     *     @OA\Parameter(
     *         name="per_page",
     *         in="query",
     *         description="Number of results per page",
     *         required=false,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="customer_type",
     *         in="query",
     *         description="Filter by customer type code",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="vehicle_use",
     *         in="query",
     *         description="Filter by vehicle use code",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="requirement",
     *         in="query",
     *         description="Filter by requirement code(s)",
     *         required=false,
     *         @OA\Schema(type="array", @OA\Items(type="string"))
     *     ),
     *     @OA\Parameter(
     *         name="contract_duration",
     *         in="query",
     *         description="Filter by contract duration code",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="min_price",
     *         in="query",
     *         description="Filter by minimum price",
     *         required=false,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Parameter(
     *         name="max_price",
     *         in="query",
     *         description="Filter by maximum price",
     *         required=false,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Parameter(
     *         name="min_autonomy",
     *         in="query",
     *         description="Filter by minimum autonomy",
     *         required=false,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Parameter(
     *         name="max_autonomy",
     *         in="query",
     *         description="Filter by maximum autonomy",
     *         required=false,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Parameter(
     *         name="sort_by",
     *         in="query",
     *         description="Sort results by field",
     *         required=false,
     *         @OA\Schema(type="string", enum={"id", "type", "code", "name", "description", "price", "autonomy", "rating", "recommendation"})
     *     ),
     *     @OA\Parameter(
     *         name="sort_order",
     *         in="query",
     *         description="Sort order (asc or desc)",
     *         required=false,
     *         @OA\Schema(type="string", enum={"asc", "desc"})
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="List of vehicles",
     *         @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Vehicle"))
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthenticated",
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden"
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $vehicles = Vehicle::query()->select('id', 'type', 'code', 'name', 'description', 'price', 'autonomy')
            ->addSelect([
                'avg_rating' => Rating::selectRaw('round(avg(rating), 1)')
                    ->whereColumn('vehicle_id', 'vehicles.id')
            ]);

        $whithRelations = (bool)$request->input('with_relations', 1);

        if ($whithRelations) {
            $vehicles->with([
                'customerTypes' => function ($query) {
                    $query->select('customer_types.id', 'customer_types.code', 'customer_types.name')
                        ->orderBy('customer_types.id');
                },
                'vehicleUses' => function ($query) {
                    $query->select('vehicle_uses.id', 'vehicle_uses.code', 'vehicle_uses.name')
                        ->orderBy('vehicle_uses.id');
                },
                'requirements' => function ($query) {
                    $query->select('requirements.id', 'requirements.code', 'requirements.name')
                        ->orderBy('requirements.id');
                },
                'contractDurations' => function ($query) {
                    $query->select('contract_durations.id', 'contract_durations.code', 'contract_durations.name')
                        ->orderBy('contract_durations.id');
                }
            ]);
        }

        $this->vehiclesFltrngAndSrtng($request, $vehicles);

        if ($request->input('paginate', 0)) {
            $vehicles = $vehicles->paginate($request->input('per_page', 10));
        } else {
            $vehicles = $vehicles->get();
        }

        foreach ($vehicles as $vehicle) {
            if ($whithRelations) {
                foreach ($vehicle->customerTypes as $customerType) {
                    $customerType->setHidden(['pivot']);
                }

                foreach ($vehicle->vehicleUses as $vehicleUse) {
                    $vehicleUse->setHidden(['pivot']);
                }

                foreach ($vehicle->requirements as $requirement) {
                    $requirement->setHidden(['pivot']);
                }

                foreach ($vehicle->contractDurations as $contractDuration) {
                    $contractDuration->setHidden(['pivot']);
                }

                $ratings = Rating::where('vehicle_id', $vehicle->id)
                    ->select('id', 'rating', 'comment', 'customer_id')
                    ->get();
                $vehicle->setRelation('ratings', $ratings);
            }
        }

        return response()->json($vehicles);
    }

    /**
     * @OA\Get(
     *     path="/api/vehicles/{id}",
     *     tags={"Vehicles"},
     *     summary="Get vehicle by ID",
     *     description="Get a specific vehicle by ID with related entities",
     *     security={{"sanctum": {}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the vehicle to retrieve",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Vehicle data",
     *         @OA\JsonContent(ref="#/components/schemas/Vehicle")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Vehicle not found"
     *     )
     * )
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $vehicle = Vehicle::with([
            'customerTypes' => function ($query) {
                $query->select('customer_types.id', 'customer_types.code', 'customer_types.name')
                    ->orderBy('customer_types.id');
            },
            'vehicleUses' => function ($query) {
                $query->select('vehicle_uses.id', 'vehicle_uses.code', 'vehicle_uses.name')
                    ->orderBy('vehicle_uses.id');
            },
            'requirements' => function ($query) {
                $query->select('requirements.id', 'requirements.code', 'requirements.name')
                    ->orderBy('requirements.id');
            },
            'contractDurations' => function ($query) {
                $query->select('contract_durations.id', 'contract_durations.code', 'contract_durations.name')
                    ->orderBy('contract_durations.id');
            }
        ])->select(['vehicles.*'])
            ->addSelect([
                'avg_rating' => Rating::selectRaw('round(avg(rating), 1)')
                    ->whereColumn('vehicle_id', 'vehicles.id')
            ])
            ->findOrFail($id);

        $ratings = Rating::where('vehicle_id', $vehicle->id)
            ->select('id', 'rating', 'comment', 'customer_id')
            ->get();
        $vehicle->setRelation('ratings', $ratings);

        foreach ($vehicle->customerTypes as $customerType) {
            $customerType->setHidden(['pivot']);
        }

        foreach ($vehicle->vehicleUses as $vehicleUse) {
            $vehicleUse->setHidden(['pivot']);
        }

        foreach ($vehicle->requirements as $requirement) {
            $requirement->setHidden(['pivot']);
        }

        foreach ($vehicle->contractDurations as $contractDuration) {
            $contractDuration->setHidden(['pivot']);
        }

        $vehicle->setHidden(['created_at', 'updated_at']);

        return response()->json($vehicle);
    }

    /**
     * @OA\Post(
     *     path="/api/vehicles/search",
     *     summary="Search for vehicles",
     *     tags={"Vehicles"},
     *     security={{"sanctum": {}}},
     *     @OA\RequestBody(
     *         description="Search parameters",
     *         required=true,
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="customer_type",
     *                 description="Filter by customer type code",
     *                 type="string"
     *             ),
     *             @OA\Property(
     *                 property="vehicle_use",
     *                 description="Filter by vehicle use code",
     *                 type="string"
     *             ),
     *             @OA\Property(
     *                 property="requirement",
     *                 description="Filter by requirement code(s)",
     *                 type="array",
     *                 @OA\Items(type="string")
     *             ),
     *             @OA\Property(
     *                 property="contract_duration",
     *                 description="Filter by contract duration code",
     *                 type="string"
     *             ),
     *             @OA\Property(
     *                 property="min_price",
     *                 description="Filter by minimum price",
     *                 type="number"
     *             ),
     *             @OA\Property(
     *                 property="max_price",
     *                 description="Filter by maximum price",
     *                 type="number"
     *             ),
     *             @OA\Property(
     *                 property="min_autonomy",
     *                 description="Filter by minimum autonomy",
     *                 type="number"
     *             ),
     *             @OA\Property(
     *                 property="max_autonomy",
     *                 description="Filter by maximum autonomy",
     *                 type="number"
     *             ),
     *             @OA\Property(
     *                 property="sort_by",
     *                 description="Sort by field (id, type, code, name, description, price, autonomy, rating, recommendation)",
     *                 type="string"
     *             ),
     *             @OA\Property(
     *                 property="sort_order",
     *                 description="Sort order (asc or desc)",
     *                 type="string"
     *             ),
     *             @OA\Property(
     *                 property="paginate",
     *                 description="Paginate results",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="per_page",
     *                 description="Number of results per page (if paginating)",
     *                 type="integer"
     *             )
     *         )
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Vehicle")
     *         )
     *     ),
     * @OA\Response(
     *         response=401,
     *         description="Unauthenticated",
     *     ),
     * @OA\Response(
     *         response=403,
     *         description="Forbidden"
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function search(Request $request): JsonResponse
    {
        $vehicles = Vehicle::query()->select('id', 'type', 'code', 'name', 'description', 'price', 'autonomy')
            ->addSelect([
                'avg_rating' => Rating::selectRaw('round(avg(rating), 1)')
                    ->whereColumn('vehicle_id', 'vehicles.id')
            ]);

        $this->vehiclesFltrngAndSrtng($request, $vehicles);

        if ($request->input('paginate', 0)) {
            $vehicles = $vehicles->paginate($request->input('per_page', 10));
        } else {
            $vehicles = $vehicles->get();
        }

        return response()->json($vehicles);
    }

    /**
     * @param Request $request
     * @param Builder $vehicles
     */
    private function vehiclesFltrngAndSrtng(Request $request, Builder $vehicles): void
    {
        if ($request->has('customer_type')) {
            $vehicles->whereHas('customerTypes', function ($query) use ($request) {
                $query->where('code', $request->input('customer_type'));
            });
        }

        if ($request->has('vehicle_use')) {
            $vehicles->whereHas('vehicleUses', function ($query) use ($request) {
                $query->where('code', $request->input('vehicle_use'));
            });
        }

        if ($request->has('requirement')) {
            $vehicles->whereHas('requirements', function ($query) use ($request) {
                $thisRequest = $request->input('requirement');
                if (is_countable($thisRequest)) {
                    $query->whereIn('code', $thisRequest);
                } else {
                    $query->where('code', $thisRequest);
                }
            });
        }

        if ($request->has('contract_duration')) {
            $vehicles->whereHas('contractDurations', function ($query) use ($request) {
                $query->where('code', $request->input('contract_duration'));
            });
        }

        if ($request->has('min_price')) {
            $vehicles->where('price', '>=', $request->input('min_price'));
        }

        if ($request->has('max_price')) {
            $vehicles->where('price', '<=', $request->input('max_price'));
        }

        if ($request->has('min_autonomy')) {
            $vehicles->where('autonomy', '>=', $request->input('min_autonomy'));
        }

        if ($request->has('max_autonomy')) {
            $vehicles->where('autonomy', '<=', $request->input('max_autonomy'));
        }

        if ($request->has('sort_by')) {
            if ($request->input('sort_by') === 'rating') {
                $vehicles->addSelect([
                    'avg_rating' => Rating::selectRaw('round(avg(rating), 1)')
                        ->whereColumn('vehicle_id', 'vehicles.id')
                ])->orderBy(
                    'avg_rating',
                    $request->input('sort_order', 'desc')
                );
            } elseif ($request->input('sort_by') === 'recommendation') {
                $vehicles->addSelect([
                    'avg_rating' => Rating::selectRaw('round(avg(rating), 1)')
                        ->whereColumn('vehicle_id', 'vehicles.id')
                ])->orderByRaw('(avg_rating / price) ' . $request->input('sort_order', 'desc'));
            } else {
                $vehicles->orderBy($request->input('sort_by'), $request->input('sort_order', 'asc'));
            }
        }
    }

}

