<?php

namespace App\Http\Controllers;

use App\Models\CustomerType;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

/**
 * Class CustomerTypeController
 * @package App\Http\Controllers
 *
 * @OA\Tag(
 *     name="CustomerTypes",
 *     description="Endpoints for managing customer types"
 * )
 */
class CustomerTypeController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/customer_types",
     *     tags={"CustomerTypes"},
     *     summary="Get list of customer types",
     *     description="Get a list of all customer types",
     *     security={{"sanctum": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="List of customer types",
     *         @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/CustomerType"))
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $customerTypes = CustomerType::select("id", "code", "name")->get();

        return response()->json($customerTypes);
    }

}

