<?php

namespace App\Http\Controllers;

use App\Models\Requirement;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

/**
 * Class RequirementController
 * @package App\Http\Controllers
 *
 * @OA\Tag(
 *     name="Requirements",
 *     description="Endpoints for managing requirements"
 * )
 */
class RequirementController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/requirements",
     *     tags={"Requirements"},
     *     summary="Get list of requirements",
     *     description="Get a list of all requirements",
     *     security={{"sanctum": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="List of requirements",
     *         @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Requirement"))
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $requirements = Requirement::select("id", "code", "name")->get();

        return response()->json($requirements);
    }

}

