<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use OpenApi\Annotations as OA;

/**
 * Class Controller
 * @package App\Http\Controllers
 *
 * @OA\Info(
 *      version="1.0.0",
 *      title="PandaApp",
 *      description="PandaGo demo app",
 *
 *      @OA\Contact(
 *          email="lorenzo.cameselle@gmail.com"
 *      ),
 *
 *     @OA\License(
 *          name="Apache 2.0",
 *          url="https://www.apache.org/licenses/LICENSE-2.0.html"
 *      )
 * )
 *
 * @OA\Server(
 *      url=L5_SWAGGER_CONST_HOST,
 *      description="Demo API Server"
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;
}

