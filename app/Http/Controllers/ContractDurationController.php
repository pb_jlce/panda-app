<?php

namespace App\Http\Controllers;

use App\Models\ContractDuration;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

/**
 * Class ContractDurationController
 * @package App\Http\Controllers
 *
 * @OA\Tag(
 *     name="ContractDurations",
 *     description="Endpoints for managing contract durations"
 * )
 */
class ContractDurationController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/contract_durations",
     *     tags={"ContractDurations"},
     *     summary="Get list of contract durations",
     *     description="Get a list of all contract durations",
     *     security={{"sanctum": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="List of contract durations",
     *         @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/ContractDuration"))
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $contractDurations = ContractDuration::select("id", "code", "name")->get();

        return response()->json($contractDurations);
    }

}

