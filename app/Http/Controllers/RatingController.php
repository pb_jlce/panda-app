<?php

namespace App\Http\Controllers;

use App\Models\Rating;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

/**
 * Class RatingController
 * @package App\Http\Controllers
 *
 * @OA\Tag(
 *     name="Ratings",
 *     description="Endpoints for managing ratings"
 * )
 */
class RatingController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/ratings",
     *     tags={"Ratings"},
     *     summary="Get list of ratings",
     *     description="Get a list of all ratings with related vehicle data",
     *     security={{"sanctum": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="List of ratings",
     *         @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Rating"))
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $ratings = Rating::select("id", "rating", "comment", "customer_id", "vehicle_id")
            ->with(["vehicle" => function ($query) {
                $query->select("id", "type", "code", "name");
            }])
            ->get();

        foreach ($ratings as $rating) {
            $rating->makeHidden('vehicle_id');
        }

        return response()->json($ratings);
    }

}

