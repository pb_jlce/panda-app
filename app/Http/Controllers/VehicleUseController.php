<?php

namespace App\Http\Controllers;

use App\Models\VehicleUse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

/**
 * Class VehicleUseController
 * @package App\Http\Controllers
 *
 * @OA\Tag(
 *   name="VehicleUses",
 *   description="Endpoints for managing vehicle uses"
 * )
 */
class VehicleUseController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/vehicle_uses",
     *     tags={"VehicleUses"},
     *     summary="Get list of vehicle uses",
     *     description="Get a list of all vehicle uses",
     *     security={{"sanctum": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="List of vehicle uses",
     *         @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/VehicleUse"))
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $vehicleUses = VehicleUse::select("id", "code", "name")->get();

        return response()->json($vehicleUses);
    }

}

