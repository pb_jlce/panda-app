<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use OpenApi\Annotations as OA;

/**
 * Class VehicleUse
 * @package App\Models
 *
 * @OA\Schema(
 *     schema="VehicleUse",
 *     type="object",
 *     @OA\Property(
 *         property="id",
 *         type="integer",
 *         description="The vehicle use's ID"
 *     ),
 *     @OA\Property(
 *         property="code",
 *         type="string",
 *         description="The vehicle use's code"
 *     ),
 *     @OA\Property(
 *         property="name",
 *         type="string",
 *         description="The vehicle use's name"
 *     )
 * )
 */
class VehicleUse extends Model
{
    use HasFactory;

    /**
     * @return BelongsToMany
     */
    public function vehicles(): BelongsToMany
    {
        return $this->belongsToMany(Vehicle::class, 'vehicle_use_vehicle');
    }

}

