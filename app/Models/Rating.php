<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use OpenApi\Annotations as OA;

/**
 * Class Rating
 * @package App\Models
 *
 * @OA\Schema(
 *     schema="Rating",
 *     type="object",
 *     @OA\Property(
 *         property="id",
 *         type="integer",
 *         description="The rating's ID"
 *     ),
 *     @OA\Property(
 *         property="vehicle_id",
 *         type="integer",
 *         description="The ID of the vehicle being rated"
 *     ),
 *     @OA\Property(
 *         property="rating",
 *         type="integer",
 *         description="The rating value"
 *     ),
 *     @OA\Property(
 *         property="comment",
 *         type="string",
 *         description="The rating comment"
 *     ),
 *     @OA\Property(
 *         property="customer_id",
 *         type="integer",
 *         description="The ID of the customer who left the rating"
 *     )
 * )
 */
class Rating extends Model
{
    use HasFactory;

    /**
     * @return BelongsTo
     */
    public function vehicle(): BelongsTo
    {
        return $this->belongsTo(Vehicle::class);
    }

}

