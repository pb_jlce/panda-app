<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use OpenApi\Annotations as OA;

/**
 * Class ContractDuration
 * @package App\Models
 *
 * @OA\Schema(
 *     schema="ContractDuration",
 *     type="object",
 *     @OA\Property(
 *         property="id",
 *         type="integer",
 *         description="The contract duration's ID"
 *     ),
 *     @OA\Property(
 *         property="code",
 *         type="string",
 *         description="The contract duration's code"
 *     ),
 *     @OA\Property(
 *         property="name",
 *         type="string",
 *         description="The contract duration's name"
 *     )
 * )
 */
class ContractDuration extends Model
{
    use HasFactory;

    /**
     * @return BelongsToMany
     */
    public function vehicles(): BelongsToMany
    {
        return $this->belongsToMany(Vehicle::class, 'contract_duration_vehicle');
    }

}

