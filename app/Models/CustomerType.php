<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use OpenApi\Annotations as OA;

/**
 * Class CustomerType
 * @package App\Models
 *
 * @OA\Schema(
 *     schema="CustomerType",
 *     type="object",
 *     @OA\Property(
 *         property="id",
 *         type="integer",
 *         description="The customer type's ID"
 *     ),
 *     @OA\Property(
 *         property="code",
 *         type="string",
 *         description="The customer type's code"
 *     ),
 *     @OA\Property(
 *         property="name",
 *         type="string",
 *         description="The customer type's name"
 *     )
 * )
 */
class CustomerType extends Model
{
    use HasFactory;

    /**
     * @return BelongsToMany
     */
    public function vehicles(): BelongsToMany
    {
        return $this->belongsToMany(Vehicle::class, 'customer_type_vehicle');
    }

}

