<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use OpenApi\Annotations as OA;

/**
 * Class Vehicle
 * @package App\Models
 *
 * @OA\Schema(
 *     schema="Vehicle",
 *     type="object",
 *     @OA\Property(
 *         property="id",
 *         type="integer",
 *         description="The vehicle's ID"
 *     ),
 *     @OA\Property(
 *         property="type",
 *         type="string",
 *         description="The vehicle's type"
 *     ),
 *     @OA\Property(
 *         property="code",
 *         type="string",
 *         description="The vehicle's code"
 *     ),
 *     @OA\Property(
 *         property="name",
 *         type="string",
 *         description="The vehicle's name"
 *     ),
 *     @OA\Property(
 *         property="description",
 *         type="string",
 *         description="The vehicle's description"
 *     ),
 *     @OA\Property(
 *         property="price",
 *         type="number",
 *         format="float",
 *         description="The vehicle's price"
 *     ),
 * @OA\Property(
 *         property="autonomy",
 *         type="number",
 *         format="float",
 *         description="The vehicle's autonomy"
 *     ),
 * @OA\Property(
 *         property="customerTypes",
 *         type="array",
 *         description="The vehicle's customer types",
 *         @OA\Items(ref="#/components/schemas/CustomerType")
 *     ),
 * @OA\Property(
 *         property="vehicleUses",
 *         type="array",
 *         description="The vehicle's uses",
 *         @OA\Items(ref="#/components/schemas/VehicleUse")
 *     ),
 * @OA\Property(
 *         property="requirements",
 *         type="array",
 *         description="The vehicle's requirements",
 *          @OA\Items(ref="#/components/schemas/Requirement")
 *     ),
 * @OA\Property(
 *         property="contractDurations",
 *         type="array",
 *         description="The vehicle's contract durations",
 *         @OA\Items(ref="#/components/schemas/ContractDuration")
 *     ),
 * @OA\Property(
 *         property="ratings",
 *         type="array",
 *         description="The vehicle's ratings",
 *         @OA\Items(ref="#/components/schemas/Rating")
 *     )
 * )
 */
class Vehicle extends Model
{
    use HasFactory;

    /**
     * @return BelongsToMany
     */
    public function customerTypes(): BelongsToMany
    {
        return $this->belongsToMany(CustomerType::class, 'customer_type_vehicle');
    }

    /**
     * @return BelongsToMany
     */
    public function vehicleUses(): BelongsToMany
    {
        return $this->belongsToMany(VehicleUse::class, 'vehicle_use_vehicle');
    }

    /**
     * @return BelongsToMany
     */
    public function requirements(): BelongsToMany
    {
        return $this->belongsToMany(Requirement::class, 'requirement_vehicle');
    }

    /**
     * @return BelongsToMany
     */
    public function contractDurations(): BelongsToMany
    {
        return $this->belongsToMany(ContractDuration::class, 'contract_duration_vehicle');
    }

    /**
     * @return HasMany
     */
    public function ratings(): HasMany
    {
        return $this->hasMany(Rating::class);
    }

}

