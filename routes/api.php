<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\VehicleUseController;
use App\Http\Controllers\RequirementController;
use App\Http\Controllers\CustomerTypeController;
use App\Http\Controllers\ContractDurationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/login', [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {
    ## User:
    Route::get('/user', [AuthController::class, 'show']);

    ## Vehicles:
    Route::get('/vehicles', [VehicleController::class, 'index']);
    Route::get('/vehicles/{id}', [VehicleController::class, 'show']);
    Route::post('/vehicles/search', [VehicleController::class, 'search']);

    ## Filters:
    Route::get('/contract_durations', [ContractDurationController::class, 'index']);
    Route::get('/customer_types', [CustomerTypeController::class, 'index']);
    Route::get('/ratings', [RatingController::class, 'index']);
    Route::get('/requirements', [RequirementController::class, 'index']);
    Route::get('/vehicle_uses', [VehicleUseController::class, 'index']);
});

