<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Class FiltersApiTest
 * @package Tests\Feature
 */
class FiltersApiTest extends TestCase
{
    public function testGetContractDurations()
    {
        $user = User::find(1);
        $token = $user->createToken('test-token')->plainTextToken;
        $response = $this->withHeaders(['Authorization' => 'Bearer ' . $token])->getJson('/api/contract_durations');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            '*' => [
                'id',
                'code',
                'name',
            ]
        ]);
    }

    public function testGetCustomerTypes()
    {
        $user = User::find(1);
        $token = $user->createToken('test-token')->plainTextToken;
        $response = $this->withHeaders(['Authorization' => 'Bearer ' . $token])->getJson('/api/customer_types');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            '*' => [
                'id',
                'code',
                'name',
            ]
        ]);
    }

    public function testGetRequirements()
    {
        $user = User::find(1);
        $token = $user->createToken('test-token')->plainTextToken;
        $response = $this->withHeaders(['Authorization' => 'Bearer ' . $token])->getJson('/api/requirements');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            '*' => [
                'id',
                'code',
                'name',
            ]
        ]);
    }

    public function testGetVehicleUses()
    {
        $user = User::find(1);
        $token = $user->createToken('test-token')->plainTextToken;
        $response = $this->withHeaders(['Authorization' => 'Bearer ' . $token])->getJson('/api/vehicle_uses');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            '*' => [
                'id',
                'code',
                'name',
            ]
        ]);
    }

}

