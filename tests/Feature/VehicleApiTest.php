<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Class VehicleApiTest
 * @package Tests\Feature
 */
class VehicleApiTest extends TestCase
{
    public function testGetVehicles()
    {
        $user = User::find(1);
        $token = $user->createToken('test-token')->plainTextToken;
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])->getJson('/api/vehicles', [
            'paginate' => 1,
            'page' => 2,
            'with_relations' => 0,
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            '*' => [
                'id',
                'type',
                'code',
                'name',
                'description',
                'price',
                'autonomy',
            ]
        ]);
    }

    public function testGetVehicle()
    {
        $user = User::find(1);
        $token = $user->createToken('test-token')->plainTextToken;
        $response = $this->withHeaders(['Authorization' => 'Bearer ' . $token])->getJson('/api/vehicles/8');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'id',
            'type',
            'code',
            'name',
            'description',
            'price',
            'autonomy',
        ]);
    }

    public function testSearchVehicles()
    {
        $user = User::find(1);
        $token = $user->createToken('test-token')->plainTextToken;
        $response = $this->withHeaders(['Authorization' => 'Bearer ' . $token])->postJson('/api/vehicles/search', [
            "customer_type" => "empresa",
            "vehicle_use" => "supermercados",
            "min_price" => 100,
            "sort_by" => "price",
            "sort_order" => "desc"
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            '*' => [
                'id',
                'type',
                'code',
                'name',
                'description',
                'price',
                'autonomy',
            ]
        ]);
    }

}

