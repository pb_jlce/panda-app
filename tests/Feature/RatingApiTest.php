<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Class RatingApiTest
 * @package Tests\Feature
 */
class RatingApiTest extends TestCase
{
    public function testGetRatings(): void
    {
        $user = User::find(1);
        $token = $user->createToken('test-token')->plainTextToken;
        $response = $this->withHeaders(['Authorization' => 'Bearer ' . $token])->getJson('/api/ratings');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            '*' => [
                'id',
                'rating',
                'comment',
                'customer_id',
                'vehicle' => [
                    'id',
                    'type',
                    'code',
                    'name',
                ]
            ]
        ]);
    }

}

