<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateInitialTables
 */
class CreateInitialTables extends Migration
{
    public function up(): void
    {
        Schema::create('customer_types', function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique();
            $table->string('name')->unique();
            $table->timestamps();
        });

        Schema::create('vehicle_uses', function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique();
            $table->string('name')->unique();
            $table->timestamps();
        });

        Schema::create('requirements', function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique();
            $table->string('name')->unique();
            $table->timestamps();
        });

        Schema::create('contract_durations', function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique();
            $table->string('name')->unique();
            $table->timestamps();
        });

        Schema::create('vehicles', function (Blueprint $table) {
            $table->id();
            $table->string('type')->index();
            $table->string('code')->unique();
            $table->string('name')->unique();
            $table->string('description');
            $table->decimal('price', 10);
            $table->decimal('autonomy', 10);
            $table->timestamps();
        });

        Schema::create('customer_type_vehicle', function (Blueprint $table) {
            $table->id();
            $table->foreignId('vehicle_id')->constrained()->onDelete('cascade');
            $table->foreignId('customer_type_id')->constrained()->onDelete('cascade');
        });

        Schema::create('vehicle_use_vehicle', function (Blueprint $table) {
            $table->id();
            $table->foreignId('vehicle_id')->constrained()->onDelete('cascade');
            $table->foreignId('vehicle_use_id')->constrained()->onDelete('cascade');
        });

        Schema::create('requirement_vehicle', function (Blueprint $table) {
            $table->id();
            $table->foreignId('vehicle_id')->constrained()->onDelete('cascade');
            $table->foreignId('requirement_id')->constrained()->onDelete('cascade');
        });

        Schema::create('contract_duration_vehicle', function (Blueprint $table) {
            $table->id();
            $table->foreignId('vehicle_id')->constrained()->onDelete('cascade');
            $table->foreignId('contract_duration_id')->constrained()->onDelete('cascade');
        });

        Schema::create('ratings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('vehicle_id')->constrained()->onDelete('cascade');
            $table->integer('rating');
            $table->string('comment');
            $table->integer('customer_id');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('ratings');
        Schema::dropIfExists('requirement_vehicle');
        Schema::dropIfExists('vehicle_use_vehicle');
        Schema::dropIfExists('customer_type_vehicle');
        Schema::dropIfExists('vehicles');
        Schema::dropIfExists('requirements');
        Schema::dropIfExists('contract_durations');
        Schema::dropIfExists('vehicle_uses');
        Schema::dropIfExists('customer_types');
    }
}

