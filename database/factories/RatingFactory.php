<?php

namespace Database\Factories;

use App\Models\Rating;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Rating>
 */
class RatingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $created_at = $this->faker->dateTimeBetween('2023-01-01', '2023-07-06');

        return [
            'vehicle_id' => $this->faker->numberBetween(1, 12),
            'rating' => $this->faker->numberBetween(3, 5),
            'comment' => $this->faker->paragraph(1),
            'customer_id' => $this->faker->numberBetween(45, 4850),
            'created_at' => $created_at,
            'updated_at' => $created_at
        ];
    }

}

