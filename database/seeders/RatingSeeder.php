<?php

namespace Database\Seeders;

use App\Models\Rating;
use Illuminate\Database\Seeder;

/**
 * Class RatingSeeder
 * @package Database\Seeders
 */
class RatingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Rating::truncate();

        Rating::factory()->count(70)->create();
    }

}

