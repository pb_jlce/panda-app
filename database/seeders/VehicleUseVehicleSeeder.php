<?php

namespace Database\Seeders;

use App\Models\Vehicle;
use App\Models\VehicleUse;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class VehicleUseVehicleSeeder
 * @package Database\Seeders
 */
class VehicleUseVehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('vehicle_use_vehicle')->truncate();

        $data = [];
        $vehicleIds = Vehicle::pluck('id')->all();
        $vehicleUseIds = VehicleUse::pluck('id')->all();

        foreach ($vehicleUseIds as $vehicleUseId) {
            if (in_array($vehicleUseId, [1, 2, 3, 4, 8])) {
                foreach ($vehicleIds as $vehicleId) {
                    if ($vehicleId !== 2) {
                        $data[] = [
                            'vehicle_id' => $vehicleId,
                            'vehicle_use_id' => $vehicleUseId
                        ];
                    }
                }
            } elseif (in_array($vehicleUseId, [5, 6])) {
                foreach ($vehicleIds as $vehicleId) {
                    if (!in_array($vehicleId, [1, 2, 3, 10, 12])) {
                        $data[] = [
                            'vehicle_id' => $vehicleId,
                            'vehicle_use_id' => $vehicleUseId
                        ];
                    }
                }
            } elseif (in_array($vehicleUseId, [7, 13])) {
                foreach ($vehicleIds as $vehicleId) {
                    if (!in_array($vehicleId, [2, 10, 12])) {
                        $data[] = [
                            'vehicle_id' => $vehicleId,
                            'vehicle_use_id' => $vehicleUseId
                        ];
                    }
                }
            } elseif (in_array($vehicleUseId, [9, 10, 11])) {
                foreach ($vehicleIds as $vehicleId) {
                    if (!in_array($vehicleId, [2, 12])) {
                        $data[] = [
                            'vehicle_id' => $vehicleId,
                            'vehicle_use_id' => $vehicleUseId
                        ];
                    }
                }
            } elseif ($vehicleUseId === 12) {
                foreach ($vehicleIds as $vehicleId) {
                    if (!in_array($vehicleId, [4, 5, 12])) {
                        $data[] = [
                            'vehicle_id' => $vehicleId,
                            'vehicle_use_id' => $vehicleUseId
                        ];
                    }
                }
            } elseif ($vehicleUseId === 14) {
                foreach ($vehicleIds as $vehicleId) {
                    if (in_array($vehicleId, [6, 7, 8, 9, 11])) {
                        $data[] = [
                            'vehicle_id' => $vehicleId,
                            'vehicle_use_id' => $vehicleUseId
                        ];
                    }
                }
            } elseif ($vehicleUseId === 15) {
                foreach ($vehicleIds as $vehicleId) {
                    if (in_array($vehicleId, [2, 7, 9, 11])) {
                        $data[] = [
                            'vehicle_id' => $vehicleId,
                            'vehicle_use_id' => $vehicleUseId
                        ];
                    }
                }
            }
        }

        DB::table('vehicle_use_vehicle')->insert($data);
    }

}

