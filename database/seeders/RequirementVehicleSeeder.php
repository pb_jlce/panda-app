<?php

namespace Database\Seeders;

use App\Models\Vehicle;
use App\Models\Requirement;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class RequirementVehicleSeeder
 * @package Database\Seeders
 */
class RequirementVehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('requirement_vehicle')->truncate();

        $data = [];
        $vehicleIds = Vehicle::pluck('id')->all();
        $requirementsIds = Requirement::pluck('id')->all();

        foreach ($requirementsIds as $requirementId) {
            if ($requirementId === 1) {
                foreach ($vehicleIds as $vehicleId) {
                    if ($vehicleId === 7) {
                        $data[] = [
                            'vehicle_id' => $vehicleId,
                            'requirement_id' => $requirementId
                        ];
                    }
                }
            } elseif ($requirementId === 2) {
                foreach ($vehicleIds as $vehicleId) {
                    if (in_array($vehicleId, [1, 2, 3, 4, 5])) {
                        $data[] = [
                            'vehicle_id' => $vehicleId,
                            'requirement_id' => $requirementId
                        ];
                    }
                }
            } elseif ($requirementId === 3) {
                foreach ($vehicleIds as $vehicleId) {
                    if (in_array($vehicleId, [3, 5, 7])) {
                        $data[] = [
                            'vehicle_id' => $vehicleId,
                            'requirement_id' => $requirementId
                        ];
                    }
                }
            }
        }

        DB::table('requirement_vehicle')->insert($data);
    }

}

