<?php

namespace Database\Seeders;

use App\Models\Vehicle;
use Illuminate\Database\Seeder;

/**
 * Class VehicleSeeder
 * @package Database\Seeders
 */
class VehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Vehicle::truncate();

        Vehicle::create([
            'type' => 'electric bicycle',
            'code' => 'GOMEEPBE',
            'name' => 'GOMEEP Bicicleta eléctrica',
            'description' => 'Bicicleta eléctrica para repartos.',
            'price' => 90,
            'autonomy' => 80,
        ]);

        Vehicle::create([
            'type' => 'electric bicycle',
            'code' => 'LTTIBIZA',
            'name' => 'Littium Ibiza',
            'description' => 'Bicicleta eléctrica diseñada y desarrollada para moverse por la ciudad.',
            'price' => 96,
            'autonomy' => 100
        ]);

        Vehicle::create([
            'type' => 'electric bicycle',
            'code' => 'BKLECOCRG',
            'name' => 'BKL Eco Cargo',
            'description' => 'Bicicleta eléctrica diseñada para el reparto de paquetería pequeña.',
            'price' => 96,
            'autonomy' => 160
        ]);

        Vehicle::create([
            'type' => 'electric scooter',
            'code' => 'HRWEK1',
            'name' => 'Horwin EK1',
            'description' => 'Scooter eléctrico inteligente para facilitar el transporte. Con componentes de alta calidad.',
            'price' => 140,
            'autonomy' => 70
        ]);

        Vehicle::create([
            'type' => 'electric scooter',
            'code' => 'HRWEK1DS',
            'name' => 'Horwin EK1 DS',
            'description' => 'Scooter eléctrico inteligente para facilitar el transporte. Con componentes de alta calidad.',
            'price' => 138,
            'autonomy' => 70
        ]);

        Vehicle::create([
            'type' => 'electric scooter',
            'code' => 'SCBCVS1',
            'name' => 'Scoobic VS1',
            'description' => 'Vehículo eléctrico que está revolucionando el transporte de reparto urbano',
            'price' => 176,
            'autonomy' => 116
        ]);

        Vehicle::create([
            'type' => 'electric scooter',
            'code' => 'SPSCCPX',
            'name' => 'Super Soco CPX',
            'description' => 'Una moto eléctrico que destaca por su bajo mantenimiento, consumo y eficiencia.',
            'price' => 198,
            'autonomy' => 130
        ]);

        Vehicle::create([
            'type' => 'electric car',
            'code' => 'FT500E',
            'name' => 'Fiat 500-e',
            'description' => 'El nuevo Fiat 500e ha recibido este galardón por su rendimiento, diseño y precio atractivo.',
            'price' => 463,
            'autonomy' => 190
        ]);

        Vehicle::create([
            'type' => 'electric car',
            'code' => 'HKNEVMX',
            'name' => 'Hyundai KONA EV Maxx',
            'description' => 'SUV con un atractivo diseño, alto rendimientoy potencia.',
            'price' => 559,
            'autonomy' => 305
        ]);

        Vehicle::create([
            'type' => 'electric van',
            'code' => 'SCMSISTR',
            'name' => 'Scoobic Mouse Isotérmico',
            'description' => 'La revolución de la distribución urbana ha llegado.',
            'price' => 639,
            'autonomy' => 176
        ]);

        Vehicle::create([
            'type' => 'electric van',
            'code' => 'OPLZFL50',
            'name' => 'Opel Zafira-e Life 50 kWh',
            'description' => 'La combinación perfecta del confort más premium con lo último en movilidad 100% eléctrica.',
            'price' => 836,
            'autonomy' => 215
        ]);

        Vehicle::create([
            'type' => 'electric van',
            'code' => 'TYPEVGXPLS',
            'name' => 'Toyota Proace Electric Van GX Plus',
            'description' => 'ENTREGA INMEDIATA',
            'price' => 919,
            'autonomy' => 230
        ]);
    }

}

