<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

/**
 * Class UserSeeder
 * @package Database\Seeders
 */
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::truncate();

        User::create([
            'name' => 'panda.user',
            'email' => 'panda.user@pandago.eco',
            'password' => bcrypt('pandatst.2023'),
            'email_verified_at' => now(),
        ]);
    }

}

