<?php

namespace Database\Seeders;

use App\Models\Vehicle;
use App\Models\ContractDuration;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class ContractDurationVehicleSeeder
 * @package Database\Seeders
 */
class ContractDurationVehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('contract_duration_vehicle')->truncate();

        $data = [];
        $vehicleIds = Vehicle::pluck('id')->all();
        $contractDrtnsIds = ContractDuration::pluck('id')->all();

        foreach ($contractDrtnsIds as $contractDrtnId) {
            if (in_array($contractDrtnId, [1, 2])) {
                foreach ($vehicleIds as $vehicleId) {
                    if (!in_array($vehicleId, [8, 9, 11])) {
                        $data[] = [
                            'vehicle_id' => $vehicleId,
                            'contract_duration_id' => $contractDrtnId
                        ];
                    }
                }
            } elseif ($contractDrtnId === 3) {
                foreach ($vehicleIds as $vehicleId) {
                    $data[] = [
                        'vehicle_id' => $vehicleId,
                        'contract_duration_id' => $contractDrtnId
                    ];
                }
            } elseif ($contractDrtnId === 4) {
                foreach ($vehicleIds as $vehicleId) {
                    if (!in_array($vehicleId, [1, 2, 3])) {
                        $data[] = [
                            'vehicle_id' => $vehicleId,
                            'contract_duration_id' => $contractDrtnId
                        ];
                    }
                }
            } elseif ($contractDrtnId === 5) {
                foreach ($vehicleIds as $vehicleId) {
                    if (in_array($vehicleId, [4, 5, 6, 8, 9, 10])) {
                        $data[] = [
                            'vehicle_id' => $vehicleId,
                            'contract_duration_id' => $contractDrtnId
                        ];
                    }
                }
            }
        }

        DB::table('contract_duration_vehicle')->insert($data);
    }

}

