<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

/**
 * Class DatabaseSeeder
 * @package Database\Seeders
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(UserSeeder::class);
        $this->call(ContractDurationSeeder::class);
        $this->call(CustomerTypeSeeder::class);
        $this->call(RequirementSeeder::class);
        $this->call(VehicleUseSeeder::class);
        $this->call(VehicleSeeder::class);
        $this->call(RatingSeeder::class);
        $this->call(ContractDurationVehicleSeeder::class);
        $this->call(CustomerTypeVehicleSeeder::class);
        $this->call(RequirementVehicleSeeder::class);
        $this->call(VehicleUseVehicleSeeder::class);
    }

}

