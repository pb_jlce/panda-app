<?php

namespace Database\Seeders;

use App\Models\CustomerType;
use Illuminate\Database\Seeder;

/**
 * Class CustomerTypeSeeder
 * @package Database\Seeders
 */
class CustomerTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        CustomerType::truncate();

        CustomerType::create([
            'code' => "empresa",
            'name' => "Empresa",
        ]);

        CustomerType::create([
            'code' => "autonomo",
            'name' => "Autónomo",
        ]);

        CustomerType::create([
            'code' => "particular",
            'name' => "Particular",
        ]);
    }

}

