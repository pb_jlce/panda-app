<?php

namespace Database\Seeders;

use App\Models\Vehicle;
use App\Models\CustomerType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class CustomerTypeVehicleSeeder
 * @package Database\Seeders
 */
class CustomerTypeVehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('customer_type_vehicle')->truncate();

        $data = [];
        $vehicleIds = Vehicle::pluck('id')->all();
        $customerTypesIds = CustomerType::pluck('id')->all();

        foreach ($customerTypesIds as $customerTypeId) {
            if ($customerTypeId === 1) {
                foreach ($vehicleIds as $vehicleId) {
                    if ($vehicleId !== 2) {
                        $data[] = [
                            'vehicle_id' => $vehicleId,
                            'customer_type_id' => $customerTypeId
                        ];
                    }
                }
            } elseif ($customerTypeId === 2) {
                foreach ($vehicleIds as $vehicleId) {
                    if (in_array($vehicleId, [7, 8, 9, 11, 12])) {
                        $data[] = [
                            'vehicle_id' => $vehicleId,
                            'customer_type_id' => $customerTypeId
                        ];
                    }
                }
            } elseif ($customerTypeId === 3) {
                foreach ($vehicleIds as $vehicleId) {
                    if (in_array($vehicleId, [2, 7, 8, 11])) {
                        $data[] = [
                            'vehicle_id' => $vehicleId,
                            'customer_type_id' => $customerTypeId
                        ];
                    }
                }
            }
        }

        DB::table('customer_type_vehicle')->insert($data);
    }

}

