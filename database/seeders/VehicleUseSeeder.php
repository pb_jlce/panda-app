<?php

namespace Database\Seeders;

use App\Models\VehicleUse;
use Illuminate\Database\Seeder;

/**
 * Class VehicleUseSeeder
 * @package Database\Seeders
 */
class VehicleUseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        VehicleUse::truncate();

        VehicleUse::create([
            'code' => 'restaurantes',
            'name' => 'Restaurantes',
        ]);

        VehicleUse::create([
            'code' => 'comercios',
            'name' => 'Comercios',
        ]);

        VehicleUse::create([
            'code' => 'logistica_ultima_millla',
            'name' => 'Logística y Última Milla',
        ]);

        VehicleUse::create([
            'code' => 'paqueteria_postal',
            'name' => 'Paquetería ligera y postal',
        ]);

        VehicleUse::create([
            'code' => 'inmobiliarias',
            'name' => 'Inmobiliarias',
        ]);

        VehicleUse::create([
            'code' => 'flota_comercial',
            'name' => 'Flota comercial',
        ]);

        VehicleUse::create([
            'code' => 'seguridad',
            'name' => 'Seguridad',
        ]);

        VehicleUse::create([
            'code' => 'reparaciones',
            'name' => 'Reparaciones',
        ]);

        VehicleUse::create([
            'code' => 'farmacias',
            'name' => 'Farmacias',
        ]);

        VehicleUse::create([
            'code' => 'supermercados',
            'name' => 'Supermercados',
        ]);

        VehicleUse::create([
            'code' => 'otros_servicios',
            'name' => 'Otros servicios profesionales',
        ]);

        VehicleUse::create([
            'code' => 'servicios_publicos',
            'name' => 'Servicios públicos',
        ]);

        VehicleUse::create([
            'code' => 'flota_privada',
            'name' => 'Flota privada de empresa',
        ]);

        VehicleUse::create([
            'code' => 'oferta_empleados',
            'name' => 'Oferta para empleados',
        ]);

        VehicleUse::create([
            'code' => 'particulares',
            'name' => 'Uso particular',
        ]);
    }

}

