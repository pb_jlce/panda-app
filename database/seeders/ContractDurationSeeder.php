<?php

namespace Database\Seeders;

use App\Models\ContractDuration;
use Illuminate\Database\Seeder;

/**
 * Class ContractDurationSeeder
 * @package Database\Seeders
 */
class ContractDurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ContractDuration::truncate();

        ContractDuration::create([
            'code' => "filter_number_412",
            'name' => 12,
        ]);
        ContractDuration::create([
            'code' => "filter_number_424",
            'name' => 24,
        ]);
        ContractDuration::create([
            'code' => "filter_number_436",
            'name' => 36,
        ]);
        ContractDuration::create([
            'code' => "filter_number_448",
            'name' => 48,
        ]);
        ContractDuration::create([
            'code' => "filter_number_460",
            'name' => 60,
        ]);

    }

}

