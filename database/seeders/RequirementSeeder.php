<?php

namespace Database\Seeders;

use App\Models\Requirement;
use Illuminate\Database\Seeder;

/**
 * Class RequirementSeeder
 * @package Database\Seeders
 */
class RequirementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Requirement::truncate();

        Requirement::create([
            'code' => 'filter_requeriments_no_7circulacion_autovia',
            'name' => 'Circulación autovía',
        ]);

        Requirement::create([
            'code' => 'filter_requeriments_no_7conducir_sin_carnet',
            'name' => 'Conducir sin carnet (AM)',
        ]);

        Requirement::create([
            'code' => 'filter_requeriments_no_7baul_de_carga',
            'name' => 'Baúl de carga',
        ]);
    }

}

